package com.example.clevertec_task4

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.clevertec_task4.remote.ATMInfoItem
import com.example.clevertec_task4.repository.Repository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MapViewModel @Inject constructor(private val repository: Repository) : ViewModel() {

    val coordinates: MutableLiveData<List<ATMInfoItem>> = MutableLiveData()

    fun getATMCoordinates() {
        viewModelScope.launch(Dispatchers.IO) {
            coordinates.postValue(
                repository.getATMInfo()
            )
        }
    }

}