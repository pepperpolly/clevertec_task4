package com.example.clevertec_task4.di

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class ATMApplication : Application() {
    companion object {
        lateinit var app: ATMApplication
    }
}