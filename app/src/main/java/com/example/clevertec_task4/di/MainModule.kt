package com.example.clevertec_task4.di

import com.example.clevertec_task4.remote.ATMApi
import com.example.clevertec_task4.remote.NetworkDataSource
import com.example.clevertec_task4.repository.Repository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class MainModule {
    @Provides
    @Singleton
    fun provideRepo(
        networkDataSource: NetworkDataSource
    ): Repository {
        return Repository(
            networkDataSource
        )
    }

    @Provides
    @Singleton
    fun provideWeatherApi(): ATMApi {
        return Retrofit.Builder()
            .baseUrl("https://belarusbank.by/api/")
            .addConverterFactory(GsonConverterFactory.create())
            .build().create(ATMApi::class.java)
    }
}