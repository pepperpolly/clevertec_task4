package com.example.clevertec_task4.remote

import retrofit2.http.GET

interface ATMApi {
    @GET("atm?city=Гомель")
    suspend fun getATMInfo(): List<ATMInfoItem>
}