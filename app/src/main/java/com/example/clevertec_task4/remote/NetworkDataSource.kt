package com.example.clevertec_task4.remote

import javax.inject.Inject

class NetworkDataSource @Inject constructor(private val atmApi: ATMApi) {

    suspend fun getATMInfo(): List<ATMInfoItem> =
        atmApi.getATMInfo()

}