package com.example.clevertec_task4.repository

import com.example.clevertec_task4.remote.ATMInfoItem
import com.example.clevertec_task4.remote.NetworkDataSource
import javax.inject.Inject

class Repository @Inject constructor(private val networkDataSource: NetworkDataSource) {
    suspend fun getATMInfo(): List<ATMInfoItem> =
        networkDataSource.getATMInfo()
}